#include <chrono>
#include <thread>
#include <vector>
#include <string>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/stl_bind.h>
#include <pybind11/numpy.h>

#include <Spinnaker.h>
#include <SpinGenApi/SpinnakerGenApi.h>


class SpinnakerDriver {
public:
    SpinnakerDriver(){
        _ptr_system = Spinnaker::System::GetInstance();
    }
    ~SpinnakerDriver(){
        if (_ptr_camera != NULL) {
            _ptr_camera->DeInit();
        }

        // Destructors
        // HACK START
        // Spinnaker adviced hack to deallocate cameras. Crashes hard otherwise.
        _ptr_camera = NULL;

        // HACK END


        _ptr_system->ReleaseInstance();
    }

    std::vector<std::string>
    listConnectedCameras() {
        std::vector<std::string> cameras;
        Spinnaker::CameraList camera_list = _ptr_system->GetCameras();
        Spinnaker::CameraPtr ptr_camera;
        unsigned int number_of_cameras = camera_list.GetSize();

        for(unsigned int cam_index = 0; cam_index < number_of_cameras; ++cam_index) {
            ptr_camera = camera_list.GetByIndex(cam_index);
            ptr_camera->Init();

            Spinnaker::GenApi::INodeMap& nodemap_tl_device = ptr_camera->GetTLDeviceNodeMap();
            Spinnaker::GenApi::CStringPtr ptr_device_serial;
            ptr_device_serial = nodemap_tl_device.GetNode("DeviceSerialNumber");
            // HACK c_str(), spinnaker uses own string format, we want a std::string, but c_str will do.
            cameras.push_back(ptr_device_serial->GetValue().c_str());

            // Must deinit to avoid release reference.
            ptr_camera->DeInit();
        }

        // Cleanup
        camera_list.Clear();
        return cameras;
    }

    void
    connect(std::string camera_serial) {
        _serial = camera_serial;
        Spinnaker::CameraList camera_list = _ptr_system->GetCameras();
        _ptr_camera = camera_list.GetBySerial(_serial);
        _ptr_camera->Init();

        _nodemap = &_ptr_camera->GetNodeMap();
        _nodemapTLDevice = &_ptr_camera->GetTLDeviceNodeMap();

        // Clean list
        camera_list.Clear();
    }

    void
    enableSoftwareTrigger() {

        _ptr_sw_trigger = _nodemap->GetNode("TriggerSoftware");

        Spinnaker::GenApi::CEnumerationPtr ptrTriggerMode = _nodemap->GetNode("TriggerMode");
        Spinnaker::GenApi::CEnumEntryPtr ptrTriggerModeOff = ptrTriggerMode->GetEntryByName("Off");
        ptrTriggerMode->SetIntValue(ptrTriggerModeOff->GetValue());
        Spinnaker::GenApi::CEnumerationPtr ptrTriggerSource = _nodemap->GetNode("TriggerSource");
        Spinnaker::GenApi::CEnumEntryPtr ptrTriggerSourceSoftware = ptrTriggerSource->GetEntryByName("Software");
        ptrTriggerSource->SetIntValue(ptrTriggerSourceSoftware->GetValue());
        Spinnaker::GenApi::CEnumEntryPtr ptrTriggerModeOn = ptrTriggerMode->GetEntryByName("On");
        ptrTriggerMode->SetIntValue(ptrTriggerModeOn->GetValue());
        Spinnaker::GenApi::CEnumerationPtr ptrAcquisitionMode = _nodemap->GetNode("AcquisitionMode");
        Spinnaker::GenApi::CEnumEntryPtr ptrAcquisitionModeContinuous = ptrAcquisitionMode->GetEntryByName("Continuous");
        int64_t acquisitionModeContinuous = ptrAcquisitionModeContinuous->GetValue();
        ptrAcquisitionMode->SetIntValue(acquisitionModeContinuous);
    }

    void
    setGain(float gain) {

        Spinnaker::GenApi::CEnumerationPtr ptr_gainAuto = _nodemap->GetNode("GainAuto");
        ptr_gainAuto->SetIntValue(ptr_gainAuto->GetEntryByName("Off")->GetValue());
        Spinnaker::GenApi::CFloatPtr ptr_gain = _nodemap->GetNode("Gain");
        ptr_gain->SetValue(0.0);
    }

    void
    setShuttertime(float shuttertime) {
        Spinnaker::GenApi::CEnumerationPtr exposureAuto = _nodemap->GetNode("ExposureAuto");
        exposureAuto->SetIntValue(exposureAuto->GetEntryByName("Off")->GetValue());
        Spinnaker::GenApi::CEnumerationPtr exposureMode = _nodemap->GetNode("ExposureMode");
        exposureMode->SetIntValue(exposureMode->GetEntryByName("Timed")->GetValue());
        Spinnaker::GenApi::CFloatPtr exposureTime = _nodemap->GetNode("ExposureTime");
        // Shuttertime is given in microseconds
        exposureTime->SetValue(shuttertime*1000);
    }

    cv::Mat
    acquireImage() {
        cv::Mat cvimage;
        _ptr_sw_trigger->Execute();
        _ptr_image = _ptr_camera->GetNextImage();
        convertToCVMat(_ptr_image, cvimage);
        // Cleanup
        _ptr_image->Release();

        // This can only be done if we make a deep copy to cvmat.
        //_ptr_image = NULL;
        return cvimage.clone();
    }

    void
    beginAcquisition() {
        _ptr_camera->BeginAcquisition();
        // Must wait some arbitrary time, usually more than 300ms.
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }

    void
    endAcquisition() {
        _ptr_camera->EndAcquisition();
    }

    void
    acquireMany(std::vector<int> stops) {
        cv::Mat cvimage;
        std::string filename;
        //std::vector<int> stops = {10, 20, 40, 80, 160, 320, 640, 1280};

        _ptr_camera->BeginAcquisition();
        std::this_thread::sleep_for(std::chrono::milliseconds(500));

        for (auto stop : stops) {

            // exposure is in microseconds
            setShuttertime(stop);

            _ptr_sw_trigger->Execute();
            _ptr_image = _ptr_camera->GetNextImage();
            convertToCVMat(_ptr_image, cvimage);
            filename = "./img_" + std::to_string(stop) + ".png";
            cv::imwrite(filename, cvimage);
            _ptr_image->Release();
        }
        _ptr_camera->EndAcquisition();
    }

private:
    Spinnaker::SystemPtr _ptr_system = NULL;
    Spinnaker::CameraPtr _ptr_camera = NULL;
    Spinnaker::ImagePtr _ptr_image;
    Spinnaker::GenApi::INodeMap* _nodemap;
    Spinnaker::GenApi::INodeMap* _nodemapTLDevice;
    Spinnaker::GenApi::CCommandPtr _ptr_sw_trigger = NULL;
    std::string _serial;
    bool _camera_is_acquiring = false;

    void
    convertToCVMat(Spinnaker::ImagePtr ptr_image, cv::Mat& pCVImg) {
        unsigned int x_padding      = ptr_image->GetXPadding();
        unsigned int y_padding      = ptr_image->GetYPadding();
        unsigned int rowsize        = ptr_image->GetWidth();
        unsigned int colsize        = ptr_image->GetHeight();
        unsigned int stride         = ptr_image->GetStride();
        unsigned int channels = ptr_image->GetBitsPerPixel();

        pCVImg = cv::Mat(colsize + y_padding,
                         rowsize + x_padding,
                         channels > 8 ? CV_8UC3 : CV_8UC1,
                         ptr_image->GetData(),
                         stride).clone();
    }

};


namespace py = pybind11;


namespace pybind11 { namespace detail {

template <> struct type_caster<cv::Mat> {
public:
    PYBIND11_TYPE_CASTER(cv::Mat, _("numpy.ndarray"));

    bool
    load(handle src, bool) {
        /* Try a default converting into a Python */
        array b(src, true);
        buffer_info info = b.request();

        int ndims = info.ndim;

        decltype(CV_32F) dtype;
        size_t elemsize;
        if (info.format == format_descriptor<float>::format()) {
            if (ndims == 3) {
                dtype = CV_32FC3;
            } else {
                dtype = CV_32FC1;
            }
            elemsize = sizeof(float);
        } else if (info.format == format_descriptor<double>::format()) {
            if (ndims == 3) {
                dtype = CV_64FC3;
            } else {
                dtype = CV_64FC1;
            }
            elemsize = sizeof(double);
        } else if (info.format == format_descriptor<unsigned char>::format()) {
            if (ndims == 3) {
                dtype = CV_8UC3;
            } else {
                dtype = CV_8UC1;
            }
            elemsize = sizeof(unsigned char);
        } else {
            throw std::logic_error("Unsupported type");
            return false;
        }

        std::vector<int> shape = {info.shape[0], info.shape[1]};

        value = cv::Mat(cv::Size(shape[1], shape[0]), dtype, info.ptr, cv::Mat::AUTO_STEP);
        return true;
    }

    static handle
    cast(const cv::Mat &m, return_value_policy, handle defval) {
        std::string format = format_descriptor<unsigned char>::format();
        size_t elemsize = sizeof(unsigned char);
        int dim;
        switch(m.type()) {
        case CV_8U:
            format = format_descriptor<unsigned char>::format();
            elemsize = sizeof(unsigned char);
            dim = 2;
            break;
        case CV_8UC3:
            format = format_descriptor<unsigned char>::format();
            elemsize = sizeof(unsigned char);
            dim = 3;
            break;
        case CV_32F:
            format = format_descriptor<float>::format();
            elemsize = sizeof(float);
            dim = 2;
            break;
        case CV_64F:
            format = format_descriptor<double>::format();
            elemsize = sizeof(double);
            dim = 2;
            break;
        default:
            throw std::logic_error("Unsupported type");
        }

        std::vector<size_t> bufferdim;
        std::vector<size_t> strides;
        if (dim == 2) {
            bufferdim = {(size_t) m.rows, (size_t) m.cols};
            strides = {elemsize * (size_t) m.cols, elemsize};
        } else if (dim == 3) {
            bufferdim = {(size_t) m.rows, (size_t) m.cols, (size_t) 3};
            strides = {(size_t) elemsize * m.cols * 3, (size_t) elemsize * 3, (size_t) elemsize};
        }
        return array(buffer_info(
                         m.data,         /* Pointer to buffer */
                         elemsize,       /* Size of one scalar */
                         format,         /* Python struct-style format descriptor */
                         dim,            /* Number of dimensions */
                         bufferdim,      /* Buffer dimensions */
                         strides         /* Strides (in bytes) for each index */
                         )).release();
    }

};
}} // namespace pybind11::detail



PYBIND11_PLUGIN(SpinnakerPyWrap) {
    py::module m("SpinnakerPyWrap", "Python wrapper for Spinnaker SDK");
    py::class_<SpinnakerDriver> sd(m, "SpinnakerDriver");
    sd.def(py::init<>());
    sd.def("connect", &SpinnakerDriver::connect);
    sd.def("getCameras", &SpinnakerDriver::listConnectedCameras);
    sd.def("enableSoftwareTrigger", &SpinnakerDriver::enableSoftwareTrigger);
    sd.def("begin", &SpinnakerDriver::beginAcquisition);
    sd.def("end", &SpinnakerDriver::endAcquisition);
    sd.def("acquireImage", &SpinnakerDriver::acquireImage, py::return_value_policy::reference);
    sd.def("setGain", &SpinnakerDriver::setGain);
    sd.def("setShuttertime", &SpinnakerDriver::setShuttertime);

#ifdef VERSION_INFO
    m.attr("__version__") = py::str(VERSION_INFO);
#else
    m.attr("__version__") = py::str("dev");
#endif

    return m.ptr();
}
