import numpy as np
import cv2
import sys
import os
import time
library_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(library_path, "build"))
import SpinnakerPyWrap


spinnaker_system = SpinnakerPyWrap.SpinnakerDriver()
print("System")
cameras = spinnaker_system.getCameras()
print("Cameras")
spinnaker_system.connect(cameras[0])
print("Camera: [{}]".format(cameras[0]))
spinnaker_system.enableSoftwareTrigger()
print("SW Trigger")
spinnaker_system.setGain(0.0)
print("Set Gain")
spinnaker_system.setShuttertime(50.0)
print("Set Shuttertime")
spinnaker_system.begin()
img = spinnaker_system.acquireImage()
spinnaker_system.end()
print("First Image acquired")
cv2.imwrite("./Image.png", img)

stops = [10, 20, 40, 80, 160, 320, 640, 1280]
images = []
spinnaker_system.begin()
t1 = time.time()
for stop in stops:
    spinnaker_system.setShuttertime(stop)
    images.append(spinnaker_system.acquireImage())

t2 = time.time()
spinnaker_system.end()
for image in images:
    cv2.imwrite("./images/stop_{}.png".format(stop), image)

print("Elapsed time: {}s".format(t2-t1))
print("Expected time: {}s".format(sum(stops) / 1000.0))
